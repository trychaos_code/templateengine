var Validator = require("jsonschema").Validator;
var { repoSchema, dataSchema } = require("./schema");
var v = new Validator();

module.exports = {
  validateData: data => v.validate(data, dataSchema),
  validateRepo: data => v.validate(data, repoSchema)
};
