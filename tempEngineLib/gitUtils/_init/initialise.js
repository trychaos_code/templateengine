const fs = require("fs"),
  async = require("async"),
  path = require("path"),
  { cloneNBuildGit } = require("../clone_git");

const initialiseRepos = bootstrapCB => {
  fs.readFile(path.resolve(__dirname, "./git_repos.json"), "utf8", function(
    err,
    contents
  ) {
    const repos = JSON.parse(contents);
    global.templateEngineConfig = {
      ...(global.templateEngineConfig || {}),
      repos
    };
    if (repos.length) {
      const cloneTaskObj = {};
      repos.forEach(
        ({ name, url, branch, buildPath, selector, buildCommand }) => {
          cloneTaskObj[url] = cb =>
            cloneNBuildGit(
              {
                name,
                url,
                branch,
                buildPath,
                selector,
                buildCommand
              },
              cb
            );
        }
      );

      async.parallel(cloneTaskObj, (err, results) => {
        console.log("err: ", err);
        bootstrapCB();
      });
    } else {
      bootstrapCB();
    }
  });
};

module.exports = {
  initialiseRepos
};
