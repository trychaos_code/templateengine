var express = require("express"),
  path = require("path"),
  app = express(),
  addApiList = require("./api_routes/apiEngine"),
  conf = require("./conf/config"),
  middleParse = require("./common/middleware.js"),
    {bootstrap} = require('tempEngineLib');

middleParse.middleware(app, express);
bootstrap(app, express, './node_modules/tempenginelib/gitUtils/gitPackages', '~/.ssh/id_rsa',()=>{
  addApiList(app);
  var srvr = app.listen(conf.development.server.port, function() {
    console.log("listening on port : " + conf.development.server.port);
  });
})
